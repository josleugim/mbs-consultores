$(function () {

});
$(document).ready(function() {
  
	//Smooth scrolling
	$('.nav li a').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });
  
  //Se activa la descripción correspondiente al elemento seleccionado
  $('#carousel-service-thin .ver-mas a').click(function(event) {
    event.preventDefault();
    $('#service-full #carousel-service-big').carousel('pause');
    var elementId = $(this).attr('id');
      $('#service-full #carousel-service-big .carousel-inner .item').removeClass('active');
      switch(elementId)
      {
        case 'facturacion':
          $('#service-full #carousel-service-big .carousel-inner #facturacion').addClass('active');
        break;
        case 'contabilidad':
          $('#service-full #carousel-service-big .carousel-inner #contabilidad').addClass('active');
        break;
        case 'auditoria':
          $('#service-full #carousel-service-big .carousel-inner #auditoria').addClass('active');
        break;
        case 'impuestos':
          $('#service-full #carousel-service-big .carousel-inner #impuestos').addClass('active');
        break;
        case 'fiscal':
          $('#service-full #carousel-service-big .carousel-inner #fiscal').addClass('active');
        break;
        case 'gestoria':
          $('#service-full #carousel-service-big .carousel-inner #gestoria').addClass('active');
        break;
      };
    $('#service').css('display','none');
    $( "#service-full" ).fadeIn( "slow", function() {
      $('#service-full').css('display','block');
    });
  });

  $('#service-full .ver-todos').click(function(event) {
    event.preventDefault();
    $('#service-full').css('display','none');
    $( "#service" ).fadeIn( "slow", function() {
      $('#service').css('display','block');
    });
  })
});